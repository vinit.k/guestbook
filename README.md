Steps to run the application:

1. Go to the root directory
2. Install necessary packages: `npm install`
2. Run the application: `node server`
3. Go to browser url: "http://localhost:8080"
4. Thanks